import React from "react"
import ReactDOM from "react-dom"
import io from "socket.io-client"
import _ from "lodash"
import moment from "moment"

const socket = io();

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			nickname: '',
            nickWrap: true,
            contentWrap: false,
			users: [],
			message: '',
			messages: [],
            alertUser: false,
			statusMessage: false,
            userWhoTypingMessage: ''
		};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.userNames = this.userNames.bind(this);
        this.submitMessage = this.submitMessage.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
        this.messagesChat = this.messagesChat.bind(this);
        this.anymessageView = this.anymessageView.bind(this);
        this.parseDateWithMomento = this.parseDateWithMomento.bind(this);
	}

	componentWillMount() {
        socket.on('usernames', this.userNames);
        socket.on('new:message', this.messagesChat);
        socket.on('status:message', this.anymessageView);
	}
    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate(prevProps, prevState) {
        this.scrollToBottom();
    }

    scrollToBottom() {
        const chatContainer = ReactDOM.findDOMNode(this.chatContainer);
        chatContainer.scrollTop = chatContainer.scrollHeight;
    }

    handleChange(event) {
        this.setState({nickname: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        let self = this;
        socket.emit('new:user', this.state.nickname, function(data){
            if(data) {
                self.setState({
					nickWrap: false,
					contentWrap: true
                });
            } else {
                self.setState({
                    alertUser: true
            	});
            }
        });
    }

    userNames(data) {
        let users = [];
		for(let i = 0; i < data.length; ++i){
			users.push(data[i]);
		}
        this.setState({
            users: users
        });
	}

	anymessageView(data) {
		let that = this;
		const {status,userWhoTypingMessage} = data;

        let timerId = setInterval(function() {
            if(data) {
                that.setState({
                    statusMessage: status,
                    userWhoTypingMessage: userWhoTypingMessage
                });
            }
        }, 200);

        setTimeout(function() {
            clearInterval(timerId);
            that.setState({
                statusMessage: false
            });
        }, 1500);
	}

    handleMessage(event) {
        this.setState({message: event.target.value});
        socket.emit('status:message', event.target.value);
    }

	submitMessage(event) {
        event.preventDefault();
		socket.emit('send:message', this.state.message);
		this.setState({
			message: ''
		});
	}

    messagesChat(data) {
        let {messages} = this.state;
        messages.push(data);
        this.setState(messages);
	}

	parseDateWithMomento(date) {
        let localTime  = moment.utc(date).toDate();//local machine time
        localTime = moment(localTime).format('HH:mm');//parse time
        return localTime;
	}

	render() {
		return(
			<div className="container-fluid" id="main">
				<div className="row">
					<div className="col-md-12">
						<h1 className="bg-info text-center">React.js live-chat with Node.js and Socket.io</h1>
					</div>
				</div>
				<div className="row" style={{display: !this.state.nickWrap ? 'none' : ''}}>
					<div className="col-md-12">
						<div className="alert alert-danger text-center"
							 style={{display: !this.state.alertUser ? 'none' : ''}}>
							<strong>This user is already in the chat. Try a different name!</strong>
						</div>
						<form onSubmit={this.handleSubmit} className="form-inline text-center login-form-padding">
							<div className="form-group">
								<label>Nickname: </label>
								<input className="form-control"
									value={this.state.nickname}
									onChange={this.handleChange}
								/>
							</div>
							<button disabled={!this.state.nickname} type="submit" className="btn btn-primary">Login</button>
						</form>
					</div>
				</div>
				<div className="row" style={{display: !this.state.contentWrap ? 'none' : ''}}>
					<div className="col-md-4 col-lg-4">
						<div className="well users">
							<h3 className="text-center">Online users:</h3>
							<ul className="list-group">
								{
									_.map(this.state.users, (value, key) => {
                                        if(this.state.nickname === value) {
											return <li className="list-group-item" key={key}>
												<i className="glyphicon glyphicon-user"/>
												<strong> {value}</strong>
												</li>
                                        } else if (this.state.userWhoTypingMessage === value) {
                                            return <li className="list-group-item" key={key}>
												<i className="glyphicon glyphicon-user"/>
                                                 {value}
												<i style={{display: !this.state.statusMessage ? 'none' : ''}}
												   className="glyphicon glyphicon-pencil pull-right"
												/>
											</li>
                                        } else {
                                            return <li className="list-group-item" key={key}>
												<i className="glyphicon glyphicon-user"/>
												 {value}
											</li>
										}
									})
								}
							</ul>
						</div>
					</div>
					<div className="col-md-8 col-lg-8 chat-border">
						<div className="well chat" ref={(el) => { this.chatContainer = el; }}>
							{
                                _.map(this.state.messages, (value, key) => {
                                    if (this.state.nickname === value.nick) {
                                        return <p className="bg-info bg-info-padding" key={key}>
											<span className="chat-date">{this.parseDateWithMomento(value.date)}</span>
											<strong>{value.nick}:</strong>
											<span className="chat-message">{value.msg}</span>
										</p>
                                    } else {
                                        return <p className="bg-info-padding" key={key}>
											<span className="chat-date">{this.parseDateWithMomento(value.date)}</span>
											<strong>{value.nick}:</strong>
											<span className="chat-message">{value.msg}</span>
										</p>
									}
                                })
							}
						</div>
						<form onSubmit={this.submitMessage} id="sendMessage">
							<div className="input-group">
								<input type="text"
									   value={this.state.message}
									   onChange={this.handleMessage}
									   className="form-control"
									   placeholder="Message"
								/>
								<div className="input-group-btn">
									<button className="btn btn-default" type="submit" disabled={!this.state.message}>
										<i className="glyphicon glyphicon-send"/>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<App/>,
	document.getElementById("root")
);
