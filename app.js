let express    = require('express');
let app        = express();
let path       = require('path');
let favicon    = require('serve-favicon');
let bodyParser = require('body-parser');
let server     = require('http').Server(app);
let io         = require('socket.io')(server);
let moment     = require('moment');
let usernames  = [];

let port = process.env.PORT || '3000';

app.set('port', port);
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
   res.sendFile('index.html');
});

io.sockets.on('connection', function (socket) {

    socket.on('new:user', function(data, callback){
        if(usernames.indexOf(data) !== -1) {
            callback(false);
        } else {
            callback(true);
            socket.username = data;
            usernames.push(socket.username);
            updateUsers();
        }
    });

    socket.on('status:message', function (data) {
        if(!socket.username) {
            return;
        }
        io.sockets.emit('status:message', {status: data.length > 0, userWhoTypingMessage: socket.username});
    });

    socket.on('send:message', function (data) {
        let timemoment = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        io.sockets.emit('new:message', {date: timemoment, msg: data, nick: socket.username});
    });

    socket.on('disconnect', function (data) {
        if(!socket.username) {
            return;
        }
        usernames.splice(usernames.indexOf(socket.username),1);
        updateUsers();
    });

    function updateUsers() {
        io.sockets.emit('usernames', usernames);
    }
});

server.listen(port, function(){
    console.log('Server run!');
});